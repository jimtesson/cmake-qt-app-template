#include <modern/lib.hpp>

#include <QApplication>
#include <QQmlApplicationEngine>
#include <fmt/format.h>

#include <iostream>
#include <vector>
#include <tuple>


int main(int argc, char** argv)
{
   std::vector<double> input = {1.2, 2.3, 3.4, 4.5};

    auto [mean, moment] = accumulate_vector(input);

    fmt::print("Mean: {}, Moment: {}\n",  mean, moment);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
